#!/bin/bash
set -e

[[ -z "$TRACE" ]] || set -x

# --help, --version
[ "$1" = "--help" ] || [ "$1" = "--version" ] && exec pdns_recursor $1

# treat everything except -- as exec cmd
[ "${1:0:2}" != "--" ] && exec "$@"

export TZ=UTC LANG=C LC_ALL=C

# prepare graceful shutdown
trap "pdns_control quit" SIGHUP SIGINT SIGTERM

# run the server
pdns_recursor "$@" &

wait
